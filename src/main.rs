extern crate telegram_bot;

#[macro_use]
extern crate chomp;
extern crate chrono;
extern crate time;
extern crate hyper;
extern crate csv;

mod rain;
mod common;

use rain::client::HandleCommand;

use telegram_bot::*;

fn main() {
    // Create bot, test simple API call and print bot information
    let api = Api::from_env("TELEGRAM_BOT_TOKEN").unwrap();
    println!("getMe: {:?}", api.get_me());
    let mut listener = api.listener(ListeningMethod::LongPoll(None));

    let parsers = vec![rain::parse::parse_message];

    // Fetch new updates via long poll method
    let res = listener.listen(|u| {
        // If the received update contains a message...
        if let Some(m) = u.message {
            let name = m.from.first_name;

            // TODO: check m.from is ID_ME

            // Match message type
            match m.msg {
                MessageType::Text(t) => {
                    // Print received text message to stdout
                    println!("<{}> {}", name, t);

                    // if t == "/exit" {
                    //     return Ok(ListeningAction::Stop);
                    // }

                    for parse_message in parsers.iter() {
                        let commands = parse_message(&t);
                        if commands.len() != 0 {
                            for command in commands.iter() {
                                let reply = command.handle();
                                try!(api.send_message(m.chat.id(),
                                                      reply,
                                                      None, None, None, None));
                            }
                            break;
                        }
                    }
                },
                _ => {}
            }
        }

        // If none of the "try!" statements returned an error: It's Ok!
        Ok(ListeningAction::Continue)
    });

    if let Err(e) = res {
        println!("An error occured: {}", e);
    }
}

use std::{mem, ptr};
use std::env;
use std::io::Read;

use chrono::prelude::*;
use time::Duration;
use hyper::Client;
use hyper::Ok as hyper_Ok;
use csv;

use rain::parse;

use common::when::{get_past_date, SECONDS};

// TODO: move to common::parse?
pub trait HandleCommand {
    fn handle(&self) -> String;
}

const RAIN:&str = "http://rainfallrecord.info/";

fn get_location(s: &'static str) -> u32 {
    if s == "Auto" {
        return 572;
    }
    if s == "Manual" {
        return 550;
    }
    return 0;
}

impl HandleCommand for parse::Rain {
    fn handle(&self) -> String {
        let location = get_location(self.name);
        let when = get_past_date(Local::now(), self.when.clone());
        let mut amount = self.amount;
        let mut msg = String::from("");

        match self.mode {
            parse::RainMode::Month => {
                let (month, month_total) = get_month_total(location, when.month());
                amount -= month_total;
                msg = format!("Total for {}: {:.1}mm\n", month, month_total);
            },

            parse::RainMode::Week => {
                let (start, end) = get_week_dates(when);
                let week_total = get_week_total(location, start, end);
                amount -= week_total;
                msg = format!("Total for week: {:.1}mm\n", week_total);
            },

            parse::RainMode::Day => {},
        }

        let client = Client::new();

        let mut res = client.post(&format!("{}locations/{}/record.json", RAIN, location))
            .body(&format!("api_key={}&record[date]={}&record[measurement]={:.1}",
                           env::var("RAIN_TOKEN").unwrap(),
                           when.format("%Y-%m-%d").to_string(),
                           amount))
            .send()
            .unwrap();

        if res.status == hyper_Ok {
            return format!("{}Logged {:.1}mm of rain for {} ({})",
                           msg, amount, when.format("%Y-%m-%d"), self.name);
        } else {
            let mut error = String::new();
            match res.read_to_string(&mut error) {
                Ok(_) => {
                    return error;
                },
                _ => {
                    return String::from("Error!");
                }
            }
        }
    }
}

fn get_month_total(location: u32, month: u32) -> (String, f64) {
    let m = (month - 1) as usize;
    let totals = get_total(location);
    let mut total = 0f64;
    for i in totals[m].amount.iter() {
        total += *i;
    }
    return (totals[m].month.clone(), total);
}

// NOTE: this won't work over the new year
fn get_week_total(location: u32, start: DateTime<Local>, end: DateTime<Local>) -> f64 {
    let totals = get_total(location);
    let mut total = 0f64;
    let ms = (start.month() - 1) as usize;
    let me = (end.month() - 1) as usize;
    if ms == me {
        for d in (start.day() - 1)..(end.day()) {
            total += totals[ms].amount[d as usize];
        }
    } else {
        for d in (start.day() - 1)..31 {
            total += totals[ms].amount[d as usize];
        }
        for d in 0..(end.day()) {
            total += totals[me].amount[d as usize];
        }
    }
    return total;
}

fn get_week_dates(when: DateTime<Local>) -> (DateTime<Local>, DateTime<Local>) {
    let weekday = when.weekday().number_from_monday() as i64;
    let start = when + Duration::seconds(weekday * -SECONDS);
    let end = start + Duration::seconds(6 * SECONDS);
    return (start, end)
}

#[derive(Debug)]
struct CsvRow {
    month: String,
    amount: [f64; 31],
}

fn get_total(location: u32) -> [CsvRow; 12] {
    let mut totals = unsafe {
        let mut array: [CsvRow; 12] = mem::uninitialized();

        for element in array.iter_mut() {
            ptr::write(element, CsvRow { month: String::from(""), amount: [0f64; 31] });
        }

        array
    };

    let client = Client::new();

    let res = client.get(&format!("{}locations/{}.csv", RAIN, location))
        .send()
        .unwrap();

    if res.status == hyper_Ok {
        let mut rdr = csv::Reader::from_reader(res).has_headers(true);
        for (m, row) in rdr.records().map(|r| r.unwrap()).enumerate() {
            totals[m].month = row[0].clone();
            for i in 0..31 {
                match row[i + 1].parse::<f64>() {
                    Ok(v) => {
                        totals[m].amount[i] = v;
                    },
                    Err(_) => {}
                }
            }
        }
    }

    return totals;
}

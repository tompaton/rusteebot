use chomp::prelude::*;
use chomp::ascii::{decimal, skip_whitespace, is_whitespace};

use common::when::{When, Days, Relative};

#[derive(Debug, PartialEq, Clone)]
pub struct Rain {
    pub amount: f64,
    pub name: &'static str,
    pub mode: RainMode,
    pub when: When,
}

#[derive(Debug, PartialEq, Clone)]
pub enum RainMode {
    Day,
    Week,
    Month,
}

// pass in Title case variant of s, will match s, or upper or lower case
// variants and return s.
// TODO: ignore case properly
fn stringIC<I: U8Input>(i: I, s: &str) -> SimpleResult<I, &str> {
    parse!{i;
           string(s.as_bytes())
           <|> string(String::from(s).to_uppercase().as_bytes())
           <|> string(String::from(s).to_lowercase().as_bytes());

           ret s}
}

fn rain_keyword<I: U8Input>(i: I) -> SimpleResult<I, &'static str> {
    stringIC(i, "Rain")
}

fn of<I: U8Input>(i: I) -> SimpleResult<I, &'static str> {
    option(i, parser!{stringIC("Of")}, "Of")
}

fn mm<I: U8Input>(i: I) -> SimpleResult<I, &'static str> {
    option(i, parser!{stringIC("Mm")}, "Mm")
}

fn real<I: U8Input>(i: I) -> SimpleResult<I, f64> {
    parse!{i;
           let val:f64 = decimal();
           token(b'.');
           let frac:f64 = decimal();

           // TODO: handle general decimal fractions, not just 10ths
           ret val + frac / 10f64
    }
}

fn real0<I: U8Input>(i: I) -> SimpleResult<I, f64> {
    parse!{i;
           token(b'.');
           let frac:f64 = decimal();

           ret frac / 10f64
    }
}

fn amount<I: U8Input>(i: I) -> SimpleResult<I, f64> {
    parse!{i;
           let val = real() <|> real0() <|> decimal();
           skip_whitespace();
           mm();

           ret val
    }
}

fn rain_first<I: U8Input>(i: I) -> SimpleResult<I, f64> {
    parse!{i;
           rain_keyword();
           skip_whitespace();
           let val = amount();

           ret val
    }
}

fn rain_last<I: U8Input>(i: I) -> SimpleResult<I, f64> {
    parse!{i;
           let val = amount();
           skip_whitespace();
           of();
           skip_whitespace();
           rain_keyword();

           ret val
    }
}

fn rain<I: U8Input>(i: I) -> SimpleResult<I, f64> {
    parse!{i;
           rain_first() <|> rain_last()
    }
}

fn auto_manual<I: U8Input>(i: I) -> SimpleResult<I, &'static str> {
    option(i, parser!{stringIC("Auto")}, "Manual")
}

fn whole_word<I: U8Input>(i: I, s: &str) -> SimpleResult<I, &str> {
    parse!{i;
           let w = stringIC(s);
           // TODO: use either() to avoid ret () which is required by or()?
           eof() <|> (satisfy(is_whitespace) >> ret ());
           ret w}
}

fn weekday<I: U8Input>(i: I, long: &'static str, short: &'static str) -> SimpleResult<I, &'static str> {
    parse!{i;
           whole_word(long)
           <|> whole_word(&long[0..3])
           <|> whole_word(short);

           ret long}
}

fn rain_when<I: U8Input>(i: I) -> SimpleResult<I, When> {
    option(i,
           parser!{(whole_word("Today") >> ret When::Offset(Relative::Today))
                   <|> (whole_word("Yesterday") >> ret When::Offset(Relative::Yesterday))
                   <|> (whole_word("Tomorrow") >> ret When::Offset(Relative::Tomorrow))
                   <|> (whole_word("Week") >> ret When::Offset(Relative::Week))
                   <|> (weekday("Monday", "M") >> ret When::Weekday(Days::Monday))
                   <|> (weekday("Tuesday", "Tu") >> ret When::Weekday(Days::Tuesday))
                   <|> (weekday("Wednesday", "W") >> ret When::Weekday(Days::Wednesday))
                   <|> (weekday("Thursday", "Th") >> ret When::Weekday(Days::Thursday))
                   <|> (weekday("Friday", "F") >> ret When::Weekday(Days::Friday))
                   <|> (weekday("Saturday", "Sa") >> ret When::Weekday(Days::Saturday))
                   <|> (weekday("Sunday", "Su") >> ret When::Weekday(Days::Sunday))},
           When::Offset(Relative::Today))
}

fn rain_mode<I: U8Input>(i: I) -> SimpleResult<I, RainMode> {
    option(i, parser!{(stringIC("Day") >> ret RainMode::Day)
                      <|> (stringIC("Week") >> ret RainMode::Week)
                      <|> (stringIC("Month") >> ret RainMode::Month)},
           RainMode::Day)
}

// TODO: all permutations of categories

fn categories0<I: U8Input>(i: I) -> SimpleResult<I, (&'static str, RainMode, When)> {
    parse!{i;
           let name = auto_manual();
           skip_whitespace();
           let mode  = rain_mode();
           skip_whitespace();
           let when  = rain_when();
           eof();

           ret (name, mode, when)
    }
}

fn categories1<I: U8Input>(i: I) -> SimpleResult<I, (&'static str, RainMode, When)> {
    parse!{i;
           let mode  = rain_mode();
           skip_whitespace();
           let name = auto_manual();
           skip_whitespace();
           let when  = rain_when();
           eof();

           ret (name, mode, when)
    }
}

fn rain_message<I: U8Input>(i: I) -> SimpleResult<I, Rain> {
    parse!{i;
           let amount = rain();
           skip_whitespace();
           let catg = categories0() <|> categories1();

           ret Rain{amount: amount,
                    name: catg.0,
                    mode: catg.1,
                    when: catg.2}
    }
}

pub fn parse_message(input: &String) -> Vec<Rain> {
    match parse_only(rain_message, input.as_bytes()) {
        Ok(result) => {
            vec![result]
        },
        _ => {
            vec![]
        }
    }
}

#[cfg(test)]
mod test {
    use chomp::prelude::*;
    use super::*;

    macro_rules! parse_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (input, amount, name, mode, when) = $value;
                    let expect = Rain{amount: amount,
                                      name: name,
                                      mode: mode,
                                      when: when};
                    assert_eq!(parse_only(rain_message, input.as_bytes()),
                               Ok(expect));
                }
            )*
        }
    }

    parse_tests! {
        parse_test_minimal: ("rain 3", 3f64, "Manual", RainMode::Day, When::Offset(Relative::Today)),
        parse_test_ignore_case: ("Rain 3", 3f64, "Manual", RainMode::Day, When::Offset(Relative::Today)),
        parse_test_real: ("rain 0.3", 0.3f64, "Manual", RainMode::Day, When::Offset(Relative::Today)),
        parse_test_no_zero: ("rain .3", 0.3f64, "Manual", RainMode::Day, When::Offset(Relative::Today)),
        parse_test_with_mm: ("rain 3mm", 3f64, "Manual", RainMode::Day, When::Offset(Relative::Today)),
        parse_test_real_with_mm: ("rain 3.5mm", 3.5f64, "Manual", RainMode::Day, When::Offset(Relative::Today)),
        parse_test_and_spaces: ("rain 3.5 mm", 3.5f64, "Manual", RainMode::Day, When::Offset(Relative::Today)),
        parse_test_ignore_mm_case: ("rain 3.5 Mm", 3.5f64, "Manual", RainMode::Day, When::Offset(Relative::Today)),
        parse_test_rain_last: ("3.5mm rain", 3.5f64, "Manual", RainMode::Day, When::Offset(Relative::Today)),
        parse_test_with_of: ("3.5mm of rain", 3.5f64, "Manual", RainMode::Day, When::Offset(Relative::Today)),
        parse_test_when: ("rain 3mm friday", 3f64, "Manual", RainMode::Day, When::Weekday(Days::Friday)),
        parse_test_month_mode: ("rain 3mm month", 3f64, "Manual", RainMode::Month, When::Offset(Relative::Today)),
        parse_test_week_mode: ("rain 3mm week", 3f64, "Manual", RainMode::Week, When::Offset(Relative::Today)),
        parse_test_name: ("3.5mm rain auto", 3.5f64, "Auto", RainMode::Day, When::Offset(Relative::Today)),
        parse_test_rain_first: ("rain 3.5mm auto", 3.5f64, "Auto", RainMode::Day, When::Offset(Relative::Today)),
        parse_test_full_first: ("rain 3mm auto month today", 3f64, "Auto", RainMode::Month, When::Offset(Relative::Today)),
        parse_test_full_last: ("3mm of rain auto month today", 3f64, "Auto", RainMode::Month, When::Offset(Relative::Today)),
        parse_test_mode_name: ("rain 3mm month auto", 3f64, "Auto", RainMode::Month, When::Offset(Relative::Today)),
    }

    #[test]
    fn rain_keyword_test() {
        assert_eq!(parse_only(rain_keyword, "rain".as_bytes()), Ok("Rain"));
        assert_eq!(parse_only(rain_keyword, "Rain".as_bytes()), Ok("Rain"));
        assert_eq!(parse_only(rain_keyword, "RAIN".as_bytes()), Ok("Rain"));
    }

    #[test]
    fn mm_test() {
        assert_eq!(parse_only(mm, "".as_bytes()), Ok("Mm"));
        assert_eq!(parse_only(mm, "mm".as_bytes()), Ok("Mm"));
        assert_eq!(parse_only(mm, "Mm".as_bytes()), Ok("Mm"));
        assert_eq!(parse_only(mm, "MM".as_bytes()), Ok("Mm"));
    }

    #[test]
    fn of_test() {
        assert_eq!(parse_only(of, "".as_bytes()), Ok("Of"));
        assert_eq!(parse_only(of, "of".as_bytes()), Ok("Of"));
        assert_eq!(parse_only(of, "Of".as_bytes()), Ok("Of"));
    }

    #[test]
    fn amount_test() {
        assert_eq!(parse_only(amount, "3".as_bytes()), Ok(3f64));
        assert_eq!(parse_only(amount, "3mm".as_bytes()), Ok(3f64));
        assert_eq!(parse_only(amount, "3 mm".as_bytes()), Ok(3f64));
        assert_eq!(parse_only(amount, "33 mm".as_bytes()), Ok(33f64));
        assert_eq!(parse_only(amount, "3.3 mm".as_bytes()), Ok(3.3f64));
        assert_eq!(parse_only(amount, "0.3 mm".as_bytes()), Ok(0.3f64));
        assert_eq!(parse_only(amount, ".3 mm".as_bytes()), Ok(0.3f64));
    }

    #[test]
    fn rain_first_test() {
        assert_eq!(parse_only(rain_first, "rain 3 mm".as_bytes()), Ok(3f64));
    }

    #[test]
    fn rain_last_test() {
        assert_eq!(parse_only(rain_last, "3 mm of rain".as_bytes()), Ok(3f64));
        assert_eq!(parse_only(rain_last, "3 mm rain".as_bytes()), Ok(3f64));
    }

    #[test]
    fn rain_test() {
        assert_eq!(parse_only(rain, "3 mm of rain".as_bytes()), Ok(3f64));
        assert_eq!(parse_only(rain, "rain 3 mm".as_bytes()), Ok(3f64));
    }

    #[test]
    fn auto_manual_test() {
        assert_eq!(parse_only(auto_manual, "".as_bytes()), Ok("Manual"));
        assert_eq!(parse_only(auto_manual, "auto".as_bytes()), Ok("Auto"));
        assert_eq!(parse_only(auto_manual, "Auto".as_bytes()), Ok("Auto"));
        assert_eq!(parse_only(auto_manual, "AUTO".as_bytes()), Ok("Auto"));
    }

    #[test]
    fn rain_mode_test() {
        assert_eq!(parse_only(rain_mode, "".as_bytes()), Ok(RainMode::Day));
        assert_eq!(parse_only(rain_mode, "day".as_bytes()), Ok(RainMode::Day));
        assert_eq!(parse_only(rain_mode, "Day".as_bytes()), Ok(RainMode::Day));
        assert_eq!(parse_only(rain_mode, "DAY".as_bytes()), Ok(RainMode::Day));
        assert_eq!(parse_only(rain_mode, "week".as_bytes()), Ok(RainMode::Week));
        assert_eq!(parse_only(rain_mode, "Week".as_bytes()), Ok(RainMode::Week));
        assert_eq!(parse_only(rain_mode, "WEEK".as_bytes()), Ok(RainMode::Week));
        assert_eq!(parse_only(rain_mode, "month".as_bytes()), Ok(RainMode::Month));
        assert_eq!(parse_only(rain_mode, "Month".as_bytes()), Ok(RainMode::Month));
        assert_eq!(parse_only(rain_mode, "MONTH".as_bytes()), Ok(RainMode::Month));
    }


    #[test]
    fn rain_when_test() {
        assert_eq!(parse_only(rain_when, "".as_bytes()), Ok(When::Offset(Relative::Today)));
        assert_eq!(parse_only(rain_when, "today".as_bytes()), Ok(When::Offset(Relative::Today)));
        assert_eq!(parse_only(rain_when, "friday".as_bytes()), Ok(When::Weekday(Days::Friday)));
        assert_eq!(parse_only(rain_when, "fri".as_bytes()), Ok(When::Weekday(Days::Friday)));
        assert_eq!(parse_only(rain_when, "f".as_bytes()), Ok(When::Weekday(Days::Friday)));
        assert_eq!(parse_only(rain_when, "tu".as_bytes()), Ok(When::Weekday(Days::Tuesday)));
        assert_eq!(parse_only(rain_when, "Sun".as_bytes()), Ok(When::Weekday(Days::Sunday)));
        assert_eq!(parse_only(rain_when, "Monday".as_bytes()), Ok(When::Weekday(Days::Monday)));
    }

}

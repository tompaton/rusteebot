use chrono::prelude::*;
use time::Duration;

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Days {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday,
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Relative {
    Today,
    Tomorrow,
    Yesterday,
    Week,
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum When {
    Weekday(Days),
    Offset(Relative),
}

fn weekday(day: Days) -> i32 {
    match day {
        Days::Monday => {0},
        Days::Tuesday => {1},
        Days::Wednesday => {2},
        Days::Thursday => {3},
        Days::Friday => {4},
        Days::Saturday => {5},
        Days::Sunday => {6},
    }
}

pub const SECONDS:i64 = 24 * 3600;

pub fn get_past_date(now: DateTime<Local>, day: When) -> DateTime<Local> {
    let offset = get_past_offset(day, now.weekday().number_from_monday() as i32) as i64;
    now + Duration::seconds(offset * -SECONDS)
}

fn get_past_offset(day: When, dow: i32) -> i32 {
    match day {
        When::Weekday(to_dow) => {
            let from_dow = dow - 1;
            ((from_dow - weekday(to_dow)) % 7 + 7) % 7
        },
        When::Offset(Relative::Yesterday) => {1},
        When::Offset(Relative::Today) => {0},
        When::Offset(Relative::Tomorrow) => {-1},
        When::Offset(Relative::Week) => {-7},
    }
}

pub fn get_future_date(now: DateTime<Local>, day: When) -> DateTime<Local> {
    let offset = get_future_offset(day, now.weekday().number_from_monday() as i32) as i64;
    now + Duration::seconds(offset * SECONDS)
}

fn get_future_offset(day: When, dow: i32) -> i32 {
    match day {
        When::Weekday(to_dow) => {
            let from_dow = dow - 1;
            return ((weekday(to_dow) - from_dow - 1) % 7 + 7) % 7 + 1;
        },
        When::Offset(Relative::Tomorrow) => {1},
        When::Offset(Relative::Today) => {0},
        When::Offset(Relative::Yesterday) => {-1},
        When::Offset(Relative::Week) => {7},
    }
}

#[cfg(test)]
mod test {
    use chrono::prelude::*;
    use super::*;

    fn check_past(day: When, offsets: [i32; 7], result: &'static str) {
        // run tests using same date
        let now = Local.ymd(2017, 4, 24).and_hms(9, 0, 0);

        let past_offsets = (0..7).map(|i| get_past_offset(day, i)).collect::<Vec<i32>>();
        assert_eq!(past_offsets, offsets);

        let date = get_past_date(now, day);
        assert_eq!(date.format("%Y-%m-%d").to_string(), result);
    }

    fn check_future(day: When, offsets: [i32; 7], result: &'static str) {
        // run tests using same date
        let now = Local.ymd(2017, 4, 24).and_hms(9, 0, 0);

        let future_offsets = (0..7).map(|i| get_future_offset(day, i)).collect::<Vec<i32>>();
        assert_eq!(future_offsets, offsets);

        let date = get_future_date(now, day);
        assert_eq!(date.format("%Y-%m-%d").to_string(), result);
    }

    #[test]
    fn past_test() {
        check_past(When::Offset(Relative::Today), [0, 0, 0, 0, 0, 0, 0], "2017-04-24");
        check_past(When::Offset(Relative::Yesterday), [1, 1, 1, 1, 1, 1, 1], "2017-04-23");
        check_past(When::Offset(Relative::Tomorrow), [-1, -1, -1, -1, -1, -1, -1], "2017-04-25");
        check_past(When::Offset(Relative::Week), [-7, -7, -7, -7, -7, -7, -7], "2017-05-01");
        check_past(When::Weekday(Days::Sunday), [0, 1, 2, 3, 4, 5, 6], "2017-04-23");
        check_past(When::Weekday(Days::Monday), [6, 0, 1, 2, 3, 4, 5], "2017-04-24");
        check_past(When::Weekday(Days::Tuesday), [5, 6, 0, 1, 2, 3, 4], "2017-04-18");
        check_past(When::Weekday(Days::Wednesday), [4, 5, 6, 0, 1, 2, 3], "2017-04-19");
        check_past(When::Weekday(Days::Thursday), [3, 4, 5, 6, 0, 1, 2], "2017-04-20");
        check_past(When::Weekday(Days::Friday), [2, 3, 4, 5, 6, 0, 1], "2017-04-21");
        check_past(When::Weekday(Days::Saturday), [1, 2, 3, 4, 5, 6, 0], "2017-04-22");
    }


    #[test]
    fn future_test() {
        check_future(When::Offset(Relative::Today), [0, 0, 0, 0, 0, 0, 0], "2017-04-24");
        check_future(When::Offset(Relative::Yesterday), [-1, -1, -1, -1, -1, -1, -1], "2017-04-23");
        check_future(When::Offset(Relative::Tomorrow), [1, 1, 1, 1, 1, 1, 1], "2017-04-25");
        check_future(When::Offset(Relative::Week), [7, 7, 7, 7, 7, 7, 7], "2017-05-01");
        check_future(When::Weekday(Days::Sunday), [7, 6, 5, 4, 3, 2, 1], "2017-04-30");
        check_future(When::Weekday(Days::Monday), [1, 7, 6, 5, 4, 3, 2], "2017-05-01");
        check_future(When::Weekday(Days::Tuesday), [2, 1, 7, 6, 5, 4, 3], "2017-04-25");
        check_future(When::Weekday(Days::Wednesday), [3, 2, 1, 7, 6, 5, 4], "2017-04-26");
        check_future(When::Weekday(Days::Thursday), [4, 3, 2, 1, 7, 6, 5], "2017-04-27");
        check_future(When::Weekday(Days::Friday), [5, 4, 3, 2, 1, 7, 6], "2017-04-28");
        check_future(When::Weekday(Days::Saturday), [6, 5, 4, 3, 2, 1, 7], "2017-04-29");
    }
}

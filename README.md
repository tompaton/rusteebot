# Rusteebot #

Experimental port of my telegram bot to Rust.

Uses the [Chomp](https://crates.io/crates/chomp) parser-combinator library, many thanks to Martin for writing it and helping me out!

Only implements commands to interact with http://rainfallrecord.info/ at this point.

Should be able to get up and going by creating an `env.sh` based on `env.sh.example` with your API keys and tokens, then run:

```
#!bash
source env.sh
cargo run
```

Run tests with `cargo test`.